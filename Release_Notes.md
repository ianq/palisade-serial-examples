12/xx/2020: PALISADE Serialization Examples v1.0 is released

* Initial prototype release 
* Tested with PALISADE v1.10.5

Comments/additions are welcome by the PALISADE team. Email us at
<contact@palisade-crypto.org>
